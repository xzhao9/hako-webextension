'use strict';

/**
  * @exports defaults 
  */
const defaults = {
    server: 'http://127.0.0.1:8341',
    passphrase: '',
    pubkey: ''
};

// var observeDOM = (function() {
//     var MutationObserver = window.MutationObserver || window.WebKitMutationObserver,
//         eventListenerSupported = window.addEventListener;
//     return function(obj, callback) {
// 	if(!obj || !obj.nodeType === 1) return; // validation
//         if( MutationObserver ){
//             // define a new observer
//             var obs = new MutationObserver(function(mutations, observer){
//                 if( mutations[0].addedNodes.length || mutations[0].removedNodes.length ){
//                     callback();
//                 }
//             });
//             // have the observer observe foo for changes in children
//             obs.observe( obj, { childList:true, subtree:true });
//         }
//         else if( eventListenerSupported ){
//             obj.addEventListener('DOMNodeInserted', callback, false);
//             obj.addEventListener('DOMNodeRemoved', callback, false);
//         }
//     };
// })();

function extractDomain(url) {
    var dummy = document.createElement("a");
    dummy.href = url;
    return dummy.hostname;
}

function Uint8ToString(u8a){
  var CHUNK_SZ = 0x8000;
  var c = [];
  for (var i=0; i < u8a.length; i+=CHUNK_SZ) {
    c.push(String.fromCharCode.apply(null, u8a.subarray(i, i+CHUNK_SZ)));
  }
  return c.join("");
}

function StringToUint8(data) { 
    var raw = window.atob(data);
    return Uint8Array.from(Array.prototype.map.call(raw,function(x) { 
            return x.charCodeAt(0); 
    })); 
};

function int_to_be_bytearray(x) {
    let bytes = new Uint8Array(4);
    bytes[3] = x & (0xff);
    x = x >> 8;
    bytes[2] = x & (0xff);
    x = x >> 8;
    bytes[1] = x & (0xff);
    x = x >> 8;
    bytes[0] = x & (0xff);
    return bytes;
}

function decode_resp(nacl, key, tag, data) {
    let decode_data = StringToUint8(data);
    let decrypt_data = nacl.crypto_stream_xor(decode_data, tag, key);
    let plaintext = new TextDecoder("utf-8").decode(decrypt_data);
    return plaintext;
}

function makeRequestBody(nacl, req_body, pass, pub) {
    let SALT_SIZE = 16;
    let passphrase_size = int_to_be_bytearray(pass.length);
    let encoded_pass = nacl.encode_utf8(pass);
    let req_body_size = int_to_be_bytearray(req_body.length);
    let encoded_req_body = nacl.encode_utf8(req_body);
    let salt = nacl.random_bytes(SALT_SIZE);
    let encoded_msg = new Uint8Array(passphrase_size.length + encoded_pass.length +
                                     req_body_size.length + encoded_req_body.length + SALT_SIZE);
    encoded_msg.set(passphrase_size);
    encoded_msg.set(encoded_pass, passphrase_size.length);
    encoded_msg.set(req_body_size, passphrase_size.length + encoded_pass.length);
    encoded_msg.set(encoded_req_body, passphrase_size.length + encoded_pass.length + req_body_size.length);
    encoded_msg.set(salt, passphrase_size.length + encoded_pass.length +
                    req_body_size.length + encoded_req_body.length);
    // Key size: 32 bytes
    // Tag size: 24 bytes

    let key = nacl.random_bytes(nacl.crypto_stream_KEYBYTES);
    let tag = nacl.crypto_stream_random_nonce(); // B: 24 bytes
    let encrypt_msg = nacl.crypto_stream_xor(encoded_msg, tag, key); // C
    let encrypt = new window.JSEncrypt();
    encrypt.setPublicKey(pub);
    let keyb64 = btoa(Uint8ToString(key));
    let encrypted_key = encrypt.getKey().encrypt(keyb64).toByteArray(); // A: 256 bytes
    if(encrypted_key.length == 257) { // remove the preceeding zero
        encrypted_key = encrypted_key.slice(1, 257);
    }
    // Send A+B+C
    let final_msg = new Uint8Array(encrypted_key.length + tag.length + encrypt_msg.length);
    final_msg.set(encrypted_key);
    final_msg.set(tag, encrypted_key.length);
    final_msg.set(encrypt_msg, encrypted_key.length + tag.length);
    let encoded_final_msg = btoa(Uint8ToString(final_msg));
    return {msg: encoded_final_msg, key: key, tag: tag};
}

function checkSubdomain(subdomain) {
    let lastdot = subdomain.lastIndexOf('.');
    if(lastdot === -1) {
	return subdomain; // AAA
    } else {
	let first_part = subdomain.substr(0, lastdot);
	let second_part = subdomain.substr(lastdot+1);
	let secondlast_dot = first_part.lastIndexOf('.');
	if(secondlast_dot === -1) {
	    return subdomain; // BBB.AAA
	} else {
	    let second_subdomain = first_part.substr(secondlast_dot+1);
	    return second_subdomain + "." + second_part; // BBB.AAA
	}
    }
} 

function getHakoDomain(base_domain, subdomain) {
    let hako_domain = "";
    if(subdomain === null || subdomain === undefined) { // if subdomain is null, just use base domain
	hako_domain = base_domain;
    } else {
	let two_layers_subdomain = checkSubdomain(subdomain);
	hako_domain = two_layers_subdomain + "." + base_domain;
    }
    return hako_domain;
}

function postSetLogin(nacl, req_addr, server, domain, login_info, callback) {
    // get domain base from domain with psl.min.js 
    let parsed_domain = psl.parse(domain);
    let base_domain = parsed_domain.domain;
    let subdomain = parsed_domain.subdomain;
    let hako_domain = getHakoDomain(base_domain, subdomain);
    let uname = login_info.username;
    let passwd = login_info.password;
    let plaintext = "";
    if(req_addr === "/updatelogin") {
	let olduname = login_info.oldusername;
	plaintext = hako_domain + " " + base_domain + " " + uname + " " + passwd + " " + olduname;
    } else {
	plaintext = hako_domain + " " + base_domain + " " + uname + " " + passwd;
    }
    let pack = makeRequestBody(nacl, plaintext, server.passphrase, server.pubkey);
    return jQuery.noConflict().ajax({
	type: 'POST',
	url: server.server + req_addr,
	data: pack.msg,
	success: function(data, status) {
	    // TODO: check return data is successful
	    let p = decode_resp(nacl, pack.key, pack.tag, data);
	    callback(login_info);
	},
	error: function(error) {
	    // TODO: set icon to be disconnected
	    alert("Set user login failed.");
	}
    });
}

function postGetLogin(nacl, server, domain, callback) {
    let parsed_domain = psl.parse(domain);
    let base_domain = parsed_domain.domain;
    let subdomain = parsed_domain.subdomain;
    let hako_domain = getHakoDomain(base_domain, subdomain);
    let pack = makeRequestBody(nacl, hako_domain, server.passphrase, server.pubkey);
    return jQuery.noConflict().ajax({
        type: 'POST',
        url: server.server + "/getlogin",
        data:  pack.msg,
        success: function(data, status) {
            let p = decode_resp(nacl, pack.key, pack.tag, data);
            callback(p);
        },
        error: function(error) {
            // TODO: set icon to disconnect
            console.log("Connection failed.");
        }
    });
}
