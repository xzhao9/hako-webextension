function LoginManager() {
    this.init();
}

LoginManager.prototype = {
    /* ------- Public interfaces ------- */
    /**
     * init
     * 
     * Initialize the Login Manager. Automatically called when service is created.
     * 
     */
    init : function () {
        this.__loginInfo = this._getLoginFields(false);
    },
    
    /* ------- Private members ------- */
    __loginInfo : null,
    get loginInfo() {
        return this.__loginInfo;
    },

    /* ------- Internal methods / callbacks for document integration ------- */
    _isElementVisible: function (element) {
        return !!( element.offsetWidth || element.offsetHeight || element.getClientRects().length );
    },
    /**
     * Returns an array of password field elements for the specified form.
     * If no password fields are found, or if more than 3 are found, null is returnted.
     * 
     * skipEmptyFields can be set to ignore password fields with no value.
     */
    _getPasswordFields: function(form, skipEmptyFields) {
        let pwFields = [];
        for (let i = 0; i < form.elements.length; i ++) {
            let elem = form.elements[i];
            if (elem.type != "password") continue;
	    if (!this._isElementVisible(elem)) continue;
            if (skipEmptyFields && !form.elements[i].value) continue;
            pwFields[pwFields.length] = {
                index: i,
                element: form.elements[i]
            };
        } // for form.elements
        if (pwFields.length === 0) {
            return null;
        } else if (pwFields.length > 3) {
            return null;
        }
        return pwFields;
    },
    
    /*
     * _getFormFields
     * 
     * Returns the username and password fields found in the form.
     * Returns: [usernameField, newPasswordField, oldPasswordField] 
     * 
     * usernameField may be null if the user is already logged in.
     * newPasswordField can't be null
     * oldPasswordField may be null. If null, the newPasswordField is just the login field. 
     * If not null, the form is apparently a change-password field, with the oldPasswordField 
     * containing the password that is being changed. */
    _getFormFields: function(form, isSubmission) {
        let pwFields = this._getPasswordFields(form, isSubmission);
        let usernameField = null;
        if (!pwFields) return [null, null, null];

        let usernameFieldCandidates = [];
        for (let i = pwFields[0].index - 1; i >= 0; i --) {
            if (form.elements[i].type.toLowerCase() === "text"
                || form.elements[i].type.toLowerCase() === "email") {
                usernameFieldCandidates.push(form.elements[i]);
            }
        }
        if(usernameFieldCandidates.length >= 1) {
		    for (var i = 0; i < usernameFieldCandidates.length; i ++) {
		        if(this._isElementVisible(usernameFieldCandidates[i])) {
			        usernameField = usernameFieldCandidates[i];
			        break;
		        }
		    }
        }
        if(!usernameField) {
            usernameField = usernameFieldCandidates[0];
        }

        if (!isSubmission || pwFields.length == 1) {
            let r = [usernameField, pwFields[0].element, null];
            return r;
        }

        // Try to figure out WTF is in the form based on the password values
        let oldPasswordField, newPasswordField;
        let pw0 = pwFields[0].element.value;   
        let pw1 = pwFields[1].element.value;
        let pw2 = pwFields[2] ? pwFields[2].element.value : null;
        if (pwFields.length == 3) {
            if (pw0 == pw1 && pw1 == pw2) { // All three password fields are equal - all new password?
                newPasswordField = pwField[0].element;
                oldPasswordField = null;
            } else if (pw0 == pw1) {
                newPasswordField = pwFields[1].element;
                oldPasswordField = pwFields[2].element;
            } else if (pw1 == pw2) {
                oldPasswordField = pwFields[0].element;
                newPasswordField = pwFields[1].element;
            } else if (pw0 == pw2) {
                
            } else { // All different: we can't tell which is the new password
                this.log("Form weird - all 3 detected password fields differ.");
                return [null, null, null];
            }
        } else { // pwFields.length == 2
            if (pw0 == pw1) { // Both pw0 and p1 are new password
                newPasswordField = pwFields[0].element;
                oldPasswordField = null;
            } else { // Assume pw1 is the new password, pw0 is the old password.
                oldPasswordField = pwField[0].element;
                newPasswordField = pwField[1].element;
            }
        }
        return [usernameField, newPasswordField, oldPasswordField];
    },
    
    _getDivUsernameField: function (pwField) {
        var parent = pwField.parentNode;
        while(parent.nodeName !== 'body') {
	        var inputs = Array.from(parent.getElementsByTagName('input'));
	        if(inputs.length == 1) {
	            parent = parent.parentNode;
	            continue;
	        }
	        var index = inputs.indexOf(pwField);
	        var usernameFieldCandidates = [];
	        for(var i = index - 1; i >= 0; i --) {
	            if(inputs[i].type.toLowerCase() === "text" || inputs[i].type.toLowerCase() === "email") {
		            usernameFieldCandidates.push(inputs[i]);
	            }
	        }
	        var usernameField = null;
	        if(usernameFieldCandidates.length >= 1) {
	            // if there are more than one usernameFields, choose the first visible one
	            for (var i = 0; i < usernameFieldCandidates.length; i ++) {
		            if(this._isElementVisible(usernameFieldCandidates[i])) {
		                usernameField = usernameFieldCandidates[i];
		                break;
		            }
	            }
	            if(usernameField === null) {
		            usernameField = usernameFieldCandidates[0];
	            }
	            return usernameField;
	        }
	        parent = parent.parentNode;
        }
        return null;
    },

    /**
     * _getDivpasswordfields
     * 
     * TODO: Recognize the old/new password fields.
     * */
    _getDivPasswordFields : function() {
        var inputs = document.getElementsByTagName('input');
        var pwFields = Array.from(inputs).filter(input => (input.type.toLowerCase() === "password" && this._isElementVisible(input)));
						 
        if(pwFields.length === 1) {
	        var usernameField = this._getDivUsernameField(pwFields[0]);
	        if(usernameField !== null) {
	            return [usernameField, pwFields[0], null];
	        } else { // Cannot find usernameField
	            return [null, null, null];
	        }
        } else if(pwFields.length > 1) { // more than 1 password input, maybe the change password page ?
	    
	        return [null, null, null];
        }
        return [null, null, null]; // no password box found
    },
    
    /**
     * _getLoginFields
     * 
     * isSubmission: form submission or page load
     * Fill all the login fields with provided user and password.
     */
    _getLoginFields : function(isSubmission) {
        let forms = document.forms;
        let loginForms = [];

        for (let i = 0; i < forms.length; i ++) {
            let form = forms[i];
            let result = this._getFormFields(form, isSubmission);
            let usernameField = result[0];
            let passwordField = result[1];
            if (passwordField === null) continue;
            let res = [usernameField, passwordField];
            if (result[2]) {
                res.push(result[2]);
            } else {
                res.push(null);
            }
            loginForms.push(res);
        } // for each form
        // If can't find any forms, fallback to find logins in div
        if (loginForms.length === 0) {
            let result = this._getDivPasswordFields();
            let usernameField = result[0];
            let passwordField = result[1];
            if (usernameField !== null && passwordField != null) {
                let res = [usernameField, passwordField];
                if (result[2]) {
                    res.push(result[2]);
                } else {
                    res.push(null);
                }
                loginForms.push(res);
            }
        }
        return loginForms;
    },
    
    _simulateInput: function(element, content) {
	// if the value is not changed, do not trigger the change events.
	if(element.value === content) {
	    return;
	}
	element.value = content;
	var evt = document.createEvent("Events");
	evt.initEvent("change", true, true);
	element.dispatchEvent(evt);
	evt = document.createEvent("Events");
	evt.initEvent("input", true, true);
	element.dispatchEvent(evt);
    },
    
    /**
     * fillPassword
     * 
     * Keep filling in the login fields until its value is set
     * to the provided user and password.
     */
    fillPassword : function(user, password) {
        let loginFields = this.loginInfo;
        for (let i = 0; i < loginFields.length; i ++) {
            if (user && loginFields[i][0]) {
		this._simulateInput(loginFields[i][0], user);
            }
            if (password && loginFields[i][1]) {
		this._simulateInput(loginFields[i][1], password);
            }
            if (user && loginFields[i][2]) {
		this._simulateInput(loginFields[i][2], user);
            }
        } // for loginFields
    },
};
