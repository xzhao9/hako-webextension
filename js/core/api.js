window.PAPI = (function() {
    var _API = {
        server: defaults.server,
        passphrase: defaults.passphrase,
        pubkey: defaults.pubkey,
        getOption: function() {
            return browser.storage.local.get(defaults);
        }
    };
    return _API;
}());

if (typeof API === "undefined") {
    var API = {};
}

API.promise = true;
API.api = browser;
