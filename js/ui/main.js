// Main UI entry function

var $j = jQuery.noConflict();

$j(document).ready(function() {
    var login = new LoginManager();
    var activeLogins = [];
    var activeLoginName = "";
    var alreadyFilled = false;
    
    function getMaxZ() {
        return Math.max.apply(null,
            $j.map($j('body *'), function (e) {
                if ($j(e).css('position') !== 'static')
                    return parseInt($j(e).css('z-index')) || 1;
            }));
    }

    function removePasswordPicker() {
        $j('.passwordPickerIframe').remove();
    }
    
    function showPasswordPicker(form) {
        let jPasswordPicker = $j('.passwordPickerIframe');
        if (jPasswordPicker.length > 1) {
            return;
        }
        
        var loginField = $j(form[0]);
        var loginFieldPos = loginField.offset();
        var loginFieldVisible = loginField.is(':visible');

        var position = $j(form[1]).position();
        var passwordField = $j(form[1]);
        var passwordFieldPos = passwordField.offset();
        var minWidth = 350;
        var passwordFieldWidth = passwordField.outerWidth();
        var passwordFieldVisible = loginField.is(':visible');
        var left = (loginFieldPos) ? loginFieldPos.left : passwordFieldPos.left;
        var top = (loginFieldPos) ? loginFieldPos.top : passwordFieldPos.top;
        var maxZ = getMaxZ();

        if (loginFieldPos && passwordFieldPos.top > loginFieldPos.top) {
            //console.log('login fields below each other')
            top = passwordFieldPos.top + passwordField.height() + 10;
        } else {
            // console.log('login fields next to each other')
            if (loginFieldPos) {
                top = top + loginField.height() + 10;
            } else {
                top = top + passwordField.height() + 10;
            }
        }
        if (!loginFieldVisible) {
            left = passwordFieldPos.left;
        }
        if (passwordFieldWidth < minWidth) {
            passwordFieldWidth = minWidth;
        }
        
        var pickerUrl = browser.runtime.getURL("../../html/picker.html");
        var picker = $j('<iframe class="passwordPickerIframe" scrolling="no" "height="385" width="' + minWidth + '" frameborder="0" src="' + pickerUrl + '"></iframe>');
        picker.css('cssText', "width: " + (passwordFieldWidth * 1.1) + "px !important;");
        picker.css('height', '385px');
        picker.css('position', 'absolute');
        picker.css('left', left);
        picker.css('z-index', maxZ+10);
        picker.css('top', top);
        $j('body').prepend($j(picker));
        $j('.passwordPickerIframe:not(:last)').remove();
    }

    function fillPasswordPicker() {
        // If we find one or more logins on the current page
        // Fill the logins in the list tab
        if (activeLogins.length != 0) {
            let childWindow = document.getElementsByClassName("passwordPickerIframe")[0].contentWindow;
            childWindow.postMessage({type: "LIST", logins: activeLogins}, "*");
        }
    }
    
    function onFormIconClick(e) {
        e.preventDefault();
        e.stopPropagation();
        let offsetX = e.offsetX;
        let offsetRight = (e.data.width - offsetX);
        if (offsetRight < e.data.height) {
            showPasswordPicker(e.data.form);
        }
    } 
    
    function createFormIcon(el, form) {
        var offset = el.offset();
        var width = el.width();
        var height = el.height() * 1;
        var margin = (el.css('margin')) ? parseInt(el.css('margin').replace('px', '')) : 0;
        var padding = (el.css('padding')) ? parseInt(el.css('padding').replace('px', '')) : 0;
        var pickerIcon = browser.extension.getURL('/icons/key.svg');
        $j(el).css('background-image', 'url("' + pickerIcon + '")');
        $j(el).css('background-repeat', 'no-repeat');
        //$j(el).css('background-position', '');
        $j(el).css('cssText', el.attr('style') + ' background-position: right 3px center !important;');
        $j(el).unbind('click', onFormIconClick);
        $j(el).click({width: width, height: height, form: form}, onFormIconClick);
    }

    function initForms() {
        // if form: then form, else, the closest common parent element
        for (let i = 0; i < login.loginInfo.length; i ++) {
            for(let j = 0; j < login.loginInfo[i].length; j ++){
                let el = $j(login.loginInfo[i][j]);
                createFormIcon(el, login.loginInfo[i]);
            }
        }
    }

    // callback from Hako request
    function fillLogin(resp) {
        let s = resp.split("\n");
        let logins = [];
	    let activeLoginId = 0;
        for(let i = 0; i < s.length; i += 1) {
	        if (s[i] !== "") {
	            let ss = s[i].split(' ');
		        let l = {
                    username: ss[0],
                    password: ss[1]
		        };
		        if(l.username === activeLoginName) {
		            activeLoginId = i;
		        }
		        logins.push(l);
	        }
        }
	    if (logins.length > 0) {
	        // guarantee that the activeLoginId is correct
	        let user = logins[activeLoginId].username;
            let password = logins[activeLoginId].password;
            alreadyFilled = true; // ready to fill the password
	        login.fillPassword(user, password);
	    }
        activeLogins = logins;
    }

    function refresh(login) {
        login.init();
        if(login.loginInfo.length != 0) {
            initForms();
            window.PAPI.getOption().then(function(result) {
                nacl_factory.instantiate(function(nacl) {
                    postGetLogin(nacl, result, document.domain, fillLogin);
                });
            });
        }
    }

    function afterAddLogin(login_info) {
	activeLogins.push({username: login_info.username,
			   password: login_info.password});
        alert("Save login successful.");
	removePasswordPicker();
    }

    function afterUpdateLogin(login_info) {
	for(let i = 0; i < activeLogins.length; i ++) {
	    if(activeLogins[i].username === login_info.oldusername) {
		activeLogins[i].username = login_info.username;
		activeLogins[i].password = login_info.password;
		activeLoginName = login_info.username;
		break;
	    }
	}
	alert("Update login successful.");
	removePasswordPicker();
    }

    function afterRmLogin(login_info) {
	let id = -1;
	for(let i = 0; i < activeLogins.length; i ++) {
	    if(activeLogins[i].username === login_info.username) {
		id = i;
		break;
	    }
	}
	if(id !== -1) {
	    activeLogins.splice(id, 1);
	}
	if(activeLoginName === login_info.username) {
	    if(activeLogins.length != 0) {
		activeLoginName = activeLogins[0].username;
	    } else {
		activeLoginName = "";
	    }
	}
	alert("Remove login successful.");
	removePasswordPicker();
    }
    
    function setLogin(req_addr, loginInfo, callback) {
        window.PAPI.getOption().then(function(result) {
            nacl_factory.instantiate(function(nacl){
                postSetLogin(nacl, req_addr, result, document.domain,
			     loginInfo, callback);
            });
        });
    }

    function fillSpecificLogin(login_name) {
	    activeLoginName = login_name;
	    removePasswordPicker();
	    refresh(login);
    }
    
    // Note: login_name may not appear in activeLogins
    // Because they may belong to different domains
    function fillSpecificLoginLocal(login_name, credential) {
	    login.fillPassword(login_name, credential);
    }

    setTimeout(function() {refresh(login);}, 500);

    var body = document.getElementsByTagName("body")[0];
    
    // Heuristic: when user click the page, the DOM of the page may also change, therefore we need a rescan.
    body.addEventListener('click', function() {
        removePasswordPicker();
        // Only fillin the login when it is not already filled
        // Wait a short period for the new DOM elements to render
        if(!alreadyFilled) {
            setTimeout(function() {
                refresh(login);
            }, 100);
        }
    });
    
    // message from the extension picker popup
    browser.runtime.onMessage.addListener(request => {
        if(request.type === "resetAlreadyFilled") {
            alreadyFilled = false;
            return;
        } else {
	        fillSpecificLoginLocal(request.activeLoginName, request.credential);
	        return Promise.resolve({response: "Successfully set login."});
        }
    });

    // message from the iframe picker popup
    window.addEventListener("message", function(e) {
        if (e.data.method === "closePicker") {
            removePasswordPicker();
        }
        if (e.data.method === "pickerReady") {
            fillPasswordPicker();
        }
        if (e.data.method === "addLogin") {
	        let loginInfo = {username: e.data.username, password: e.data.password};
            alreadyFilled = false;
            setLogin("/addlogin", loginInfo, afterAddLogin);
        }
	    if (e.data.method === "editLogin") {
	        let loginInfo = {oldusername: e.data.oldusername,
			                 username: e.data.username, password: e.data.password};
            alreadyFilled = false;
	        setLogin("/updatelogin", loginInfo, afterUpdateLogin);
	    }
	    if (e.data.method === "rmLogin") {
	        let loginInfo = {username: e.data.username, password: "PLACEHOLDER"};
            alreadyFilled = false;
	        setLogin("/rmlogin", loginInfo, afterRmLogin);
	    }
	    if (e.data.method === "fillLogin") {
	        fillSpecificLogin(e.data.username);
	    }
    });
});
