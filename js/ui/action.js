$(document).ready(function() {
    let picker = $('#password-picker');
    let default_settings = {
	'length': 16,
	'upperCase': true,
	'lowerCase': true,
	'digit': true,
	'specialChar': true,
	'requireEveryCharType': true
    };

    let goption;
    let gdomain;
    let logins = [];

    browser.storage.local.get(defaults).then(function(option) {
	goption = option;
	actionMain();
    });

    function debug(data) {
	jQuery(".tab-list-content").html(data);
    }
    
    function fillListPage(logins) {
	if(logins.length === 0) {
	    return;
	}
        picker.find('.tab-list-content').html("");
        for(let i = 0; i < logins.length; i ++) {
            let account_prefix = '<div class="account" data="' + logins[i].username + '">';
            let account_suffix = '<button class="small-btn remove-login pull-right">削</button> <button class="small-btn edit-login pull-right">改</button><button class="small-btn copy-login pull-right">存</button></div>';

            let account_mid = '<span class="login-name">' + logins[i].username + "</span>";
            if(i !== logins.length - 1) {
                account_suffix += "<hr />";
            }
            let full_account = account_prefix + account_mid + account_suffix;
            picker.find('.tab-list-content').append(full_account);
        }
	bindSmallBtns();
    }
    
    function fillLogin(resp) {
        let s = resp.split("\n");
        logins = [];
        for(let i = 0; i < s.length; i += 1) {
	    if (s[i] !== "") {
	    let ss = s[i].split(' ');
		let l = {
                    username: ss[0],
                    password: ss[1]
		};
		logins.push(l);
	    }
        }
	fillListPage(logins);
    }

    function afterUpdateLogin(resp) {
        browser.tabs.query({currentWindow: true, active: true}).then(function(tabInfo) {
		    browser.tabs.sendMessage(tabInfo[0].id, {type: "resetAlreadyFilled"}).then(function() {
                window.close();
            }) });
    }

    function afterRmLogin(resp) {
        browser.tabs.query({currentWindow: true, active: true}).then(function(tabInfo) {
		    browser.tabs.sendMessage(tabInfo[0].id, {type: "resetAlreadyFilled"}).then(
                function() {
	                window.close();
                }
            ); });
    }

    function afterAddLogin(resp) {
        browser.tabs.query({currentWindow: true, active: true}).then(function(tabInfo) {
		    browser.tabs.sendMessage(tabInfo[0].id, {type: "resetAlreadyFilled"}).then(function(){
	            window.close();
            }); });
    }

    function afterFillSpecificLogin() {
	    window.close();
    }

    function addLogin(login) {
	    nacl_factory.instantiate(function(nacl) {
	        postSetLogin(nacl, "/addlogin", goption, gdomain, login, afterAddLogin);
	    });
    }
    
    function getLogins(domain) {
	    nacl_factory.instantiate(function(nacl) {
	        postGetLogin(nacl, goption, domain, fillLogin);
	    });
    }

    function editLogin(login) {
	    nacl_factory.instantiate(function(nacl) {
	        postSetLogin(nacl, "/updatelogin", goption, gdomain, login, afterUpdateLogin);
	    });
    }

    function removeLogin(login) {
	    nacl_factory.instantiate(function(nacl) {
	        postSetLogin(nacl, "/rmlogin", goption, gdomain, login, afterRmLogin);
	    });
    }

    function hideTab(name) {
	$(".tab." + name).hide();
    }

    function actionMain() {
	let currentTab = browser.tabs.query({currentWindow: true, active: true});
	currentTab.then(function(tabInfo) {
	    let url = tabInfo[0].url;
	    if(!url.startsWith("http")) {
		hideTab("add");
		return;
	    }
	    let domain = extractDomain(url);
	    gdomain = domain;
	    getLogins(domain);
	}, function(error) {
	    debug("Get tab information error.");
	});
    }
    
    function makeTabActive(name) {
        // early return if the tab is already active
        if (picker.find('.tab.'+name).hasClass('active')) {
            return;
        }
        picker.find('.tab').removeClass('active');
        picker.find('.tab-content').children().hide();
        picker.find('.tab-'+name+"-content").show();
        picker.find('.tab.'+name).addClass('active');
    };

    function genAvaiableChars(u, l, n, s, settings) {
	let dic = [];
	if(settings.upperCase) {
	    dic.push(u);
	}
	if(settings.lowerCase) {
	    dic.push(l);
	}
	if(settings.digit) {
	    dic.push(n);
	}
	if(settings.specialChar) {
	    dic.push(s);
	}
	return dic;
    }

    // generate a random integer from [low, high] (inclusive)
    function genRandomInt(low, high) {
	return Math.floor(Math.random() * (high - low + 1)) + low;
    }
    
    function genLengths(total, part_cnt) {
	if(part_cnt == 1) {
	    return [total];
	} else {
	    let cur_length = genRandomInt(1, total - part_cnt + 1);
	    let sublength = genLengths(total - cur_length, part_cnt - 1);
	    return [cur_length].concat(sublength);
	}
    }

    function genRandomPermutation(n) {
	if(n == 1) {
	    return [0];
	} else {
	    // [0]
	    let subperm = genRandomPermutation(n-1);
	    let index = genRandomInt(0, n-1);
	    // console.log(subperm);
	    if(index === 0) {
		return [n-1].concat(subperm);
	    } else if (index === (n-1)) {
		return subperm.concat([n-1]);
	    } else {
		let prefix = subperm.slice(0, index);
		let suffix = subperm.slice(index);
		let half = prefix.concat([n-1]);
		let output = half.concat(suffix);
		return output;	
	    }
	}
    }

    function generateRandomPassword(settings) {
	let u = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	let l = "abcdefghijklmnopqrstuvwxyz";
	let n = "1234567890";
	let s = "!@#$%^&*()+=-_[]{}<>,.?~`";
	let length = settings.length;
	let dic = genAvaiableChars(u, l, n, s, settings);
	let lengths = genLengths(length, dic.length);
	console.log(lengths);
	let raw = "";
	let output = "";
	for(let i = 0; i < lengths.length; i ++) {
	    for(let j = 0; j < lengths[i]; j ++) {
		let random_ind = genRandomInt(0, dic[i].length-1);
		raw += dic[i][random_ind];
	    }
	}
	let perm = genRandomPermutation(length);

	for(let i = 0; i < length; i ++) {
	    output += raw[perm[i]];
	}
	console.log(output);
	return output;
    }
    
    function getParentInputPasswd(nicebutton) {
	return nicebutton.parent().prev();
    };
    
    function copyToClipboard(text) {
	var dummy = document.createElement("textarea");
	document.body.appendChild(dummy);
	dummy.value = text;
	dummy.select();
	document.execCommand("copy");
	document.body.removeChild(dummy);
    }

    function toggleFieldType(field) {
	let $ = jQuery;
	if ($(field).attr('type').toLowerCase() === 'text') {
            $(field).attr('type', 'password');
        } else {
            $(field).attr('type', 'text');
	}
    }
    
    function toggleFieldText(field, text1, text2) {
	let $ = jQuery;
	if ($(field).html() === text1) {
            $(field).html(text2);
        } else {
            $(field).html(text1);
	}
    }

    picker.find('.niceInputButtons .copypw').click(function() {
	let $ = jQuery;
	let parentPasswd = getParentInputPasswd($(this));
	let passwd = parentPasswd.val();
	copyToClipboard(passwd);
    });

    picker.find('.niceInputButtons .showpw').click(function() {
	let $ = jQuery;
	let parentPasswd = getParentInputPasswd($(this));
	toggleFieldType(parentPasswd);
	toggleFieldText($(this), "阴", "阳");
    });

    picker.find('.niceInputButtons .renewpw').click(function() {
	let local_settings = default_settings;
	let pwd_settings = jQuery(this).parent().parent().next();
	let len = pwd_settings.find(".pwg-length").val();
	let upper = pwd_settings.find(".pwg-upper").is(":checked");
	let lower = pwd_settings.find(".pwg-lower").is(":checked");
	let digit = pwd_settings.find(".pwg-digit").is(":checked");
	let special = pwd_settings.find(".pwg-special").is(":checked");
	local_settings.length = len;
	local_settings.upperCase = upper;
	local_settings.lowerCase = lower;
	local_settings.digit = digit;
	local_settings.specialChar = special;
	let passwd = generateRandomPassword(local_settings);
	let parentPasswd = getParentInputPasswd(jQuery(this));
	parentPasswd.val(passwd);
    });
    
    picker.find('.tab').click(function() {
        var name = jQuery(this).attr('data-name');
        makeTabActive(name);
    });

    picker.find('.tab.close').click(function() {
	window.close();
    });
    picker.find('#add-cancel').click(function() {
	window.close();
    });

    picker.find('#add-save').click(function() {
	let $ = jQuery;
        let uname = $('#add-username').val();
        let passwd = $('#add-passwd').val();
	uname = $.trim(uname);
	passwd = $.trim(passwd);
	addLogin({username: uname, password: passwd});
    });

    picker.find("#edit-back").click(function() {
	let $ = jQuery;
	$('.tab-list-content').show();
	$('.tab-edit-content').hide();
    });

    picker.find("#edit-save").click(function() {
	let $ = jQuery;
	let old_login_name = $(this).attr("t");
	let new_login_name = $('#edit-username').val();
	let new_login_pass = $('#edit-passwd').val();
	new_login_name = $.trim(new_login_name);
	new_login_pass = $.trim(new_login_pass);
	let find = false;
	for(let i = 0; i < logins.length; i ++) {
	    if(logins[i].username === new_login_name) {
		if(logins[i].password === new_login_pass) {
		    find = true;
		}
	    }
	}
	if(!find) {
	    editLogin({username: new_login_name, password: new_login_pass,
		       oldusername: old_login_name});
	} else {
	    // alert("Login information is not changed.");
	}
    });

    function bindSmallBtns() {
	picker.find('.remove-login').click(function() {
	    let login_name = jQuery(this).parent().attr("data");
	    removeLogin({username: login_name, password: "PLACEHOLDER"});
	});

	picker.find('.edit-login').click(function() {
	    let $ = jQuery;
	    let old_login_name = $(this).parent().attr("data");
	    let old_login_pass = "";
	    for(let i = 0; i < logins.length; i ++) {
		if(logins[i].username === old_login_name) {
		    old_login_pass = logins[i].password;
		    break;
		}
	    }
	    // set old login name and pass
	    $('#edit-username').val(old_login_name);
	    $('#edit-passwd').val(old_login_pass);
	    $('#edit-save').attr("t", old_login_name);
	    // hide existing login ui and show udpate ui
	    $('.tab-list-content').hide();
	    $('.tab-edit-content').show();
	});
	
	picker.find('.copy-login').click(function() {
	    let login_name = jQuery(this).parent().attr("data");
	    for(let i = 0; i < logins.length; i ++) {
		if(logins[i].username === login_name) {
		    copyToClipboard(logins[i].password);
		    break;
		}
	    }
	});

	picker.find('.login-name').click(function() {
	    let login_name = jQuery(this).parent().attr("data");
	    let password = "";
	    for(let i = 0; i < logins.length; i ++) {
		    if(logins[i].username === login_name) {
		        password = logins[i].password;
		    }
	    }
	    browser.tabs.query({currentWindow: true, active: true}).then(function(tabInfo) {
		browser.tabs.sendMessage(tabInfo[0].id,
					             {type: "fillSpecificlogin",
                                  activeLoginName: login_name,
					              credential: password}).then(afterFillSpecificLogin);
	    });
	});
    }
});

