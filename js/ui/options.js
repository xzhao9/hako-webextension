let server = "http://127.0.0.1:8341";

/** 
 * This method is used to load the current options. */
async function load() {
    const option = await browser.storage.local.get(defaults);
    $("#passphrase").val(option.passphrase);
    $('#publickey').val(option.pubkey);
}

document.addEventListener('DOMContentLoaded', load);

$('#save').click(function() {
    browser.storage.local.set({
        passphrase: $("#passphrase").val(),
        pubkey: $('#publickey').val()
    });
});

$('#test').click(function() {
    let pass = $("#passphrase").val();
    let pub = $('#publickey').val();
    let SALT_SIZE = 16;
    nacl_factory.instantiate(function (nacl) {
        let passphrase_size = int_to_be_bytearray(pass.length);
        let req_body = "TestConnection";
        let encoded_pass = nacl.encode_utf8(pass);
        let req_body_size = int_to_be_bytearray(req_body.length);
        let encoded_req_body = nacl.encode_utf8(req_body);
        let salt = nacl.random_bytes(SALT_SIZE);
        let encoded_msg = new Uint8Array(passphrase_size.length + encoded_pass.length +
                                        req_body_size.length + encoded_req_body.length + SALT_SIZE);
        encoded_msg.set(passphrase_size);
        encoded_msg.set(encoded_pass, passphrase_size.length);
        encoded_msg.set(req_body_size, passphrase_size.length + encoded_pass.length);
        encoded_msg.set(encoded_req_body, passphrase_size.length + encoded_pass.length + req_body_size.length);
        encoded_msg.set(salt, passphrase_size.length + encoded_pass.length +
                        req_body_size.length + encoded_req_body.length);
        // Key size: 32 bytes
        // Tag size: 24 bytes
        let key = nacl.random_bytes(nacl.crypto_stream_KEYBYTES);
        let tag = nacl.crypto_stream_random_nonce(); // B: 24 bytes
        let encrypt_msg = nacl.crypto_stream_xor(encoded_msg, tag, key); // C
        let encrypt = new JSEncrypt();
        encrypt.setPublicKey(pub);
        let keyb64 = btoa(Uint8ToString(key));
        let encrypted_key = encrypt.getKey().encrypt(keyb64).toByteArray(); // A: 256 bytes
        if(encrypted_key.length == 257) { // remove the preceeding zero
            encrypted_key = encrypted_key.slice(1, 257);
        }
        // Send A+B+C
        let final_msg = new Uint8Array(encrypted_key.length + tag.length + encrypt_msg.length);
        final_msg.set(encrypted_key);
        final_msg.set(tag, encrypted_key.length);
        final_msg.set(encrypt_msg, encrypted_key.length + tag.length);
        let encoded_final_msg = btoa(Uint8ToString(final_msg));
        $.ajax({
            type: 'POST',
            url: server + "/getlogin",
            data:  encoded_final_msg,
            success: function(data, status) {
                let p = decode_resp(nacl, key, tag, data);
                if (p === "OK") {
                    alert("Connection OK.");
                } else {
                    alert("Connection failed.");
                }
            },
            error: function(error) {
                // TODO: set icon to disconnected
                alert("Connection failed.");
            }
        });
    });
});

